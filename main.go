package okolog

import (
	"fmt"
	"runtime"
)

func HandleError(input error, line int) {
	_, _, lineNumber, _ := runtime.Caller(1)
	reterr := fmt.Sprintf("Error in %s at line %d: %s\n", runFuncName(), lineNumber, input)
	fmt.Println(reterr)
}

func runFuncName() string {
	pc := make([]uintptr, 1)
	runtime.Callers(3, pc)
	f := runtime.FuncForPC(pc[0])
	return f.Name()
}
